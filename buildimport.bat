@echo on
setlocal

rem set PRODUCT_PATH=%~dp0..

if %1""=="" goto noparam

SHIFT /0

rem @call datapower-gradle-template/mqsisetmqenv

"%JAVA_HOME%\bin\java" -cp ./datapower-gradle-template/DataPowerJavaUtil.jar com.ibm.wdp.ImportClass %*
goto end

:noparam

"%JAVA_HOME%\bin\java" com.ibm.broker.config.util.Deploy -help

:end
endlocal
